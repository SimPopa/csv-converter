
class EventEmitter {
    constructor() {
        this.listeners = {};
    }

    addListener(eventName, fn) {
        return this.on(eventName, fn);
    };

    on(eventName, fn) {
        if (!this.listeners[eventName]) {
            this.listeners[eventName] = [];
          }
          this.listeners[eventName].push(fn);
          return this;
    };

    removeListener(eventName, fn) {
        return this.off(eventName, fn);
    };

    off(eventName, fn) {
        const listenersFunctionsList = this.listeners[eventName];
        if(!listenersFunctionsList) {
            return this;
        }
        const index = listenersFunctionsList.indexOf(fn);
        if (index !== -1) {
            listenersFunctionsList.splice(index, 1);
        }
        return this;
    };

    once(eventName, fn) {
        const wrapper = (...args) => {
            fn.apply(this, args);
            this.off(eventName, wrapper);
          };
        this.on(eventName, wrapper);
        return this;
    };

    emit(eventName,...args) {
        const eventListeners = this.listeners[eventName];
        if (eventListeners) {
            eventListeners.forEach(fn => fn.apply(this, args));
        }
        return this;
    };

    listenerCount(eventName) {
        const eventListeners = this.listeners[eventName];
        return eventListeners.length
    };

    rawListeners(eventName) {
        const eventListeners = this.listeners[eventName];
        return eventListeners ? eventListeners.map(fn => fn.toString()) : [];
    };


}

export default EventEmitter;

