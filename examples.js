import { EventEmitter } from 'events';
const eventEmitter = new EventEmitter();

// listen to the event
eventEmitter.on('myEvent', () => {
    console.log('Data Received');
});

// publish an event
eventEmitter.emit('myEvent');
console.log(eventEmitter.rawListeners("myEvent"));

// Create buffer from string
let buffFromString = Buffer.from("Nigeria");
console.log(buffFromString);

// Create buffer from the buffer(which we created from string)
let buffFromBuffer = Buffer.from(buffFromString);
console.log(buffFromBuffer);

// create a buffer from an array
const buffFromArray = Buffer.from([0x62, 0x75, 0x66, 0x66, 0x65, 0x72]);
console.log(buffFromArray);


// Create a buffer from an arraybuffer
const buffFromArrayBuffer = new ArrayBuffer(10);
console.log(buffFromArrayBuffer);
// Specify offset and length
const buffSpecifyOffsetAndLength = Buffer.from(buffFromArrayBuffer, 0, 2);
console.log(buffSpecifyOffsetAndLength);