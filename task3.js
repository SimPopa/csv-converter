import csvtojson from 'csvtojson';
import fs from 'fs';
import readline from 'readline';


const csvFilePath = 'csv/csv-file.csv';
const txtFilePath = 'txt/txt-file.txt';

const convertCSVtoJSON = () => {
    //emppties the txt file
    fs.writeFileSync(txtFilePath, '');
    return new Promise((resolve, reject) => {
        const readStream = fs.createReadStream(csvFilePath);
        const rl = readline.createInterface({
            input: readStream,
            crlfDelay: Infinity
        });

        let headersRow = '';

        rl.on('line', async (line) => {
            try {
                if (headersRow === '') {
                    headersRow = line
                    return;
                }
                const txtData = await csvtojson().fromString(headersRow + '\n' + line);
                fs.appendFileSync(txtFilePath, JSON.stringify(txtData[0]) + '\n')
            } catch (error) {
                console.error('Error converting line to JSON:', error);
                reject(error);
            }
        });

        rl.on('close', () => {
            console.log('CSV content successfully written to TXT file.');
            resolve();
        });
    });
};

const runConversion = async () => {
    try {
        await convertCSVtoJSON();
    } catch (error) {
        console.error('An error occurred:', error);
    }
};

runConversion();