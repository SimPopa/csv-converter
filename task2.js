import EventEmitter from './task1.js';
import fetch from 'node-fetch';

class WithTime extends EventEmitter {
    execute(asyncFunc, ...args) {
       // emit event start, end, data received
       // call asyncFunc with args specified
       // compute the time it takes to execute asyncFunc
        this.emit('begin');
        console.time('execute time');
        this.on('data', (data)=> console.log('got data ', data));
        asyncFunc(...args, (err, data) => {
          if (err) {
            return this.emit('error', err);
          }
          this.emit('data', data);
          console.timeEnd('execute time');
          this.emit('end');
        });
      }
 }
 
 const fetchFromUrl = (url, cb) => {
    fetch(url)
      .then((resp) => resp.json())
      .then(function(data) {
        cb(null, data);
      });
  }
 
 const withTime = new WithTime();
 
 withTime.on('begin', () => console.log('About to execute'));
 withTime.on('end', () => console.log('Done with execute'));
 
 withTime.execute(fetchFromUrl, 'https://jsonplaceholder.typicode.com/posts/1');
 
 console.log(withTime.rawListeners("end"));